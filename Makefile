.PHONY: build
build:
	go build -o $$(go env GOPATH)/bin/tfs-auction-client cmd/tfs-auction-client/main.go

.PHONY: lint
lint:
	golangci-lint run -c .golangci.yml ./...

.PHONY: test
test:
	go test -v -race -count=1 ./...
