package apperr

import "github.com/pkg/errors"

var ErrWrongResponseCode = errors.New("wrong response code")
