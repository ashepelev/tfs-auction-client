package suits

import (
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"runtime"
	"strings"

	"gitlab.com/vadimlarionov/tfs-auction-client/internal/httpclient"
	"gitlab.com/vadimlarionov/tfs-auction-client/internal/requests"
	"gitlab.com/vadimlarionov/tfs-auction-client/internal/responses"
)

const (
	signUpURL = "/v1/auction/signup"
	signInURL = "/v1/auction/signin"
)

type TestSuite struct {
	baseURL string
	client  *httpclient.Client
	passed  int
	failed  int
}

func (s *TestSuite) run(f func() error) {
	name := runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name()
	splitted := strings.Split(name, ".")
	name = splitted[len(splitted)-1]
	name = strings.TrimRight(name, "-fm")

	fmt.Printf("=== RUN %s\n", name)
	if err := f(); err != nil {
		fmt.Printf("--- FAIL %s\n%s\n", name, err)
		s.failed++
	} else {
		fmt.Printf("--- PASS %s\n", name)
		s.passed++
	}
}

// nolint:unparam
func (s *TestSuite) signUpSuccess(email, password, birthday string) error {
	return httpclient.Request{}.
		To(s.baseURL+signUpURL, http.MethodPost).
		WithRequest(requests.Register{
			FirstName: "Rob",
			LastName:  "Pike",
			Email:     email,
			Password:  password,
			Birthday:  birthday,
		}).
		Execute(s.client).
		ExpectCode(http.StatusCreated).
		Validate()
}

func (s *TestSuite) signInSuccess(email, password string) (*responses.Token, error) {
	resp := httpclient.Request{}.
		To(s.baseURL+signInURL, http.MethodPost).
		WithRequest(requests.Authorize{
			Email:    email,
			Password: password,
		}).
		Execute(s.client).
		ExpectCode(http.StatusOK)
	if err := resp.Validate(); err != nil {
		return nil, fmt.Errorf("invalid sign in response:\n%s", err)
	}

	var tokenResponse responses.Token
	if err := json.Unmarshal([]byte(resp.Body), &tokenResponse); err != nil {
		return nil, fmt.Errorf("can't unmarshal response: %s", err)
	}

	if tokenResponse.TokenType != "bearer" {
		return nil, fmt.Errorf("invalid token_type %q, response=%+v", tokenResponse.TokenType, resp)
	}

	if tokenResponse.AccessToken == "" {
		return nil, fmt.Errorf("empty token, response=%+v", resp)
	}

	return &tokenResponse, nil
}
