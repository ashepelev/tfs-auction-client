package suits

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/vadimlarionov/tfs-auction-client/internal/requests"
)

const (
	testPassword = "test_password"
)

func TestAuthTestSuite_SignUpSignIn(t *testing.T) {
	r := require.New(t)
	const testToken = "test_bearer_token"

	var email string
	mux := http.NewServeMux()
	mux.HandleFunc(signUpURL, func(w http.ResponseWriter, req *http.Request) {
		r.Equal(http.MethodPost, req.Method)
		var registerReq requests.Register
		readJSONBody(t, req.Body, &registerReq)

		r.Equal("Rob", registerReq.FirstName)
		r.Equal("Pike", registerReq.LastName)
		r.Equal(testPassword, registerReq.Password)
		r.NotEmpty(registerReq.Email)
		email = registerReq.Email

		w.WriteHeader(http.StatusCreated)
	})
	mux.HandleFunc(signInURL, func(w http.ResponseWriter, req *http.Request) {
		r.Equal(http.MethodPost, req.Method)
		var authReq requests.Authorize
		readJSONBody(t, req.Body, &authReq)

		r.Equal(email, authReq.Email)
		r.Equal(testPassword, authReq.Password)

		_, err := fmt.Fprintf(w, `{"token_type":"bearer","access_token":"%s"}`, testToken)
		r.NoError(err)
	})

	ts := httptest.NewServer(mux)

	suite := NewAuthTestSuite(ts.URL, time.Second)
	err := suite.SignUpSignIn()
	r.NoError(err)
}

func TestAuthTestSuite_SignUpDuplEmail(t *testing.T) {
	r := require.New(t)

	var counter int
	mux := http.NewServeMux()
	mux.HandleFunc(signUpURL, func(w http.ResponseWriter, req *http.Request) {
		var registerReq requests.Register
		readJSONBody(t, req.Body, &registerReq)
		counter++

		r.Equal("Rob", registerReq.FirstName)
		r.Equal("Pike", registerReq.LastName)
		r.Equal(testPassword, registerReq.Password)
		r.NotEmpty(registerReq.Email)

		if counter == 1 {
			w.WriteHeader(http.StatusCreated)
			return
		}

		w.WriteHeader(http.StatusConflict)
	})

	ts := httptest.NewServer(mux)

	suite := NewAuthTestSuite(ts.URL, time.Second)
	r.NoError(suite.SignUpDuplEmail())
}

func readJSONBody(t *testing.T, reader io.Reader, out interface{}) {
	body, err := ioutil.ReadAll(reader)
	require.NoError(t, err)
	err = json.Unmarshal(body, out)
	require.NoError(t, err)
}

func TestAuthTestSuite_SignUpEmptyEmail(t *testing.T) {
	testBadRequest(t, signUpURL, http.MethodPost, func(baseURL string) error {
		suite := NewAuthTestSuite(baseURL, time.Second)
		return suite.SignUpEmptyEmail()
	})
}

func TestAuthTestSuite_SignUpEmptyPassword(t *testing.T) {
	testBadRequest(t, signUpURL, http.MethodPost, func(baseURL string) error {
		suite := NewAuthTestSuite(baseURL, time.Second)
		return suite.SignUpEmptyPassword()
	})
}

func testBadRequest(t *testing.T, pattern, expectedHTTPMethod string, f func(baseURL string) error) {
	r := require.New(t)
	mux := http.NewServeMux()
	mux.HandleFunc(pattern, func(w http.ResponseWriter, req *http.Request) {
		r.Equal(expectedHTTPMethod, req.Method)
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"error":"something error"}`)
	})

	ts := httptest.NewServer(mux)
	r.NoError(f(ts.URL))
}

func TestAuthTestSuite_SignInUnknownUser(t *testing.T) {
	r := require.New(t)
	mux := http.NewServeMux()
	mux.HandleFunc(signInURL, func(w http.ResponseWriter, req *http.Request) {
		r.Equal(http.MethodPost, req.Method)
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprintf(w, `{"error":"can't something: invalid email or password"}`)
	})

	ts := httptest.NewServer(mux)
	suite := NewAuthTestSuite(ts.URL, time.Second)
	r.NoError(suite.SignInUnknownUser())
}
